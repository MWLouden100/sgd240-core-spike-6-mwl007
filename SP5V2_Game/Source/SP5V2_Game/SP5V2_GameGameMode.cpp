﻿// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "SP5V2_GameGameMode.h"
#include "SP5V2_GameHUD.h"
#include "Kismet/GameplayStatics.h"
#include "SP5V2_GameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASP5V2_GameGameMode::ASP5V2_GameGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ASP5V2_GameHUD::StaticClass();
}

void ASP5V2_GameGameMode::BeginPlay()
{
	Super::BeginPlay();

	SetCurrentState(EGamePlayState::EPlaying);

	MyCharacter = Cast<ASP5V2_GameCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
}

void ASP5V2_GameGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (MyCharacter)
	{
		if (FMath::IsNearlyZero(MyCharacter->GetHealth(), 0.001f))
		{
			SetCurrentState(EGamePlayState::EGameOver);
		}
	}
}

EGamePlayState ASP5V2_GameGameMode::GetCurrentState() const
{
	return CurrentState;
}

void ASP5V2_GameGameMode::SetCurrentState(EGamePlayState NewState)
{
	CurrentState = NewState;
	HandleNewState(CurrentState);
}

void ASP5V2_GameGameMode::HandleNewState(EGamePlayState NewState)
{
	switch (NewState)
	{
		case EGamePlayState::EPlaying:
		{
			//Game is Playing - Nothing to do here
		}
		break;
		
		case EGamePlayState::EGameOver:
		{
			UGameplayStatics::OpenLevel(this, FName(*GetWorld()->GetName()), false);
		}
		break;

		default:
		case EGamePlayState::EUnknown:
		{
			// ¯\_(ツ)_/¯ - do nothing
		}
		break;
	}
}
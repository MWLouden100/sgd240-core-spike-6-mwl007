// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SP5V2_GameCharacter.h"
#include "SP5V2_GameGameMode.generated.h"

UENUM()
enum class EGamePlayState
{
	EPlaying,
	EGameOver,
	EUnknown
};

UCLASS(minimalapi)
class ASP5V2_GameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ASP5V2_GameGameMode();

	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	ASP5V2_GameCharacter* MyCharacter;

	UFUNCTION(BlueprintPure, Category = "Health")
		EGamePlayState GetCurrentState() const;

	void SetCurrentState(EGamePlayState NewState);

private:

	EGamePlayState CurrentState;

	void HandleNewState(EGamePlayState NewState);

};



